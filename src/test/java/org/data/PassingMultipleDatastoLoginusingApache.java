package org.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PassingMultipleDatastoLoginusingApache {

	public static void main(String[] args) throws IOException {
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		File f = new File("/home/jeril/Documents/TestData2.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook bk = new XSSFWorkbook(fis);
		XSSFSheet sh = bk.getSheetAt(2);
		int rows =sh.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {
			String username= sh.getRow(i).getCell(0).getStringCellValue();
			String password = sh.getRow(i).getCell(1).getStringCellValue();
			driver.findElement(By.linkText("Log in")).click();
			driver.findElement(By.id("Email")).sendKeys(username);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.xpath("//input[@value='Log in']")).click();
			 driver.findElement(By.linkText("Log out")).click();
	
		}
		
	}

}
