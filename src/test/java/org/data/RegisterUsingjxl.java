package org.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class RegisterUsingjxl {
public static void main(String[] args) throws IOException, BiffException {
	WebDriver driver= new ChromeDriver();
	driver.get("https://demowebshop.tricentis.com/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	File f = new File("/home/jeril/Documents/TestData1.xls");
	FileInputStream fis = new FileInputStream(f);
	Workbook book = Workbook.getWorkbook(fis);
	Sheet sh=book.getSheet("register");
	
	
	int rows = sh.getRows();
	int columns = sh.getColumns();
	
	for (int i = 1; i < rows; i++) {
		String firstName= sh.getCell(0,i).getContents();
		String lastName = sh.getCell(1,i).getContents();
		String email = sh.getCell(2,i).getContents();
		String password = sh.getCell(3,i).getContents();
		String confirmpassword = sh.getCell(4,i).getContents();
		driver.findElement(By.className("ico-register")).click();
		driver.findElement(By.id("gender-male")).click();
		driver.findElement(By.id("FirstName")).sendKeys(firstName);
		driver.findElement(By.id("LastName")).sendKeys(lastName);
		driver.findElement(By.id("Email")).sendKeys(email);
		driver.findElement(By.id("Password")).sendKeys(password);
		driver.findElement(By.id("ConfirmPassword")).sendKeys(confirmpassword);
		driver.findElement(By.id("register-button")).click();
}
}
}
