package org.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FetchingDataThroughApachePoi {

	public static void main(String[] args) throws IOException {
		File f = new File("/home/jeril/Documents/TestData2.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook bk = new XSSFWorkbook(fis);
		XSSFSheet sh = bk.getSheetAt(0);
		int rows = sh.getPhysicalNumberOfRows();
		for (int i = 0; i < rows; i++) {
			int columns = sh.getRow(i).getLastCellNum();
			for (int j = 0; j < columns; j++) {
				String cellvalue = sh.getRow(i).getCell(j).getStringCellValue();
				System.out.println(cellvalue);
			}
		}
	}

}
