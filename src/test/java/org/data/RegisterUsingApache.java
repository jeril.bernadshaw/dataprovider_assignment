package org.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class RegisterUsingApache {

	public static void main(String[] args) throws BiffException, IOException {
		WebDriver driver= new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		File f = new File("/home/jeril/Documents/Test.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook bk = new XSSFWorkbook(fis);
		XSSFSheet sh = bk.getSheetAt(0);
		int rows =sh.getPhysicalNumberOfRows();
		for (int i = 1; i < rows; i++) {
		String firstName=sh.getRow(i).getCell(0).getStringCellValue();
		String lastName=sh.getRow(i).getCell(1).getStringCellValue();
		String email=sh.getRow(i).getCell(2).getStringCellValue();
		String password =sh.getRow(i).getCell(3).getStringCellValue();
		
		driver.findElement(By.className("ico-register")).click();
		driver.findElement(By.id("gender-male")).click();
		driver.findElement(By.id("FirstName")).sendKeys(firstName);
		driver.findElement(By.id("LastName")).sendKeys(lastName);
		driver.findElement(By.id("Email")).sendKeys(email);
		driver.findElement(By.id("Password")).sendKeys(password);
		driver.findElement(By.id("ConfirmPassword")).sendKeys(password);
		driver.findElement(By.id("register-button")).click();
	}

}
}
